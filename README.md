# Pawood

Pawood is a [Hugo](https://gohugo.io) theme. It displays a really simple
visual elements **gallery** with **infinite scroll** and **diaporama**. It
has been designed for visual artits willing to give a simple and elegant
preview of their work.

**Pawood** is not designed to handle other content type than portfolio pictures,
such as blog posts. You can create static pages, like a Contact page.

## Installation

Inside your main **Hugo** project directory:

```shell
git submodule --add https://framagit.org/Ximun/hugo-pawood-theme themes/pawood
```

## Configuration

### Theme activation

```toml
theme = "pawood"
```

### Options

Pawood is giving you some ways to customize your project. It is necessary to
write them under the `[params]` key inside your main `config.toml` config file.

| option | default value | description |
|--------|---------------|-------------|
| showDescription | *false* | Display information into diaporama |
| showCategories | *true* | Display categories into image information |
| showTags | *true* | Display tags into image information |
| galleryZoomable | *true* | Users can click on image gallery to zoom on it |

## Manage content

### Visual element

For each new visual element into the portoflio, 3 steps are necessary:

- Create a markdown file containing element description.
- Put the fullsize picture into the project.
- Put the thumbnail picture.

Please respect the data structure or elements will not display correctly. Put
the fullsize picture into the `content/portfolio/images/full` directory and
the thumbnail into `content/portfolio/images/thumb`.

```shell
content/portfolio/
├── _index.md
├── name-of-your-work.md
└── images
    ├── full
    │   └── name-of-your-work.jpg
    └── thumb
        └── name-of-your-work.jpg
```
The image namefile has to be the same into `full` and `thumb` directories.
Then, simply create a new markdown file thanks to the Hugo command:

```shell
hugo new portfolio/name-of-your-work.md
```

Fill the correct name for picture, write some description if you need and
don't forget to undraft your production.

```shell
# content/portfolio/name-of-your-work.md
---
title: "Name of Your Work"
date: 2021-02-26T01:45:56+01:00
image: name-of-your-work.jpg
categories: [ what, you, want ]
tags: [ the, same ]
draft: false
---

Write some descriptions if you want.  Title and description will be displayed
in diaporama if `showDescription` option is set to `true`.
```

### Contact Page

Contact page has a particular template page:

- Information is centered.
- The template displays social links from `config.toml`.

The contact follows some rules to be correctly displayed, and displays

```shell
content/contact
└── _index.md
```

Contacts are filled into `_index.md`. Don't forget two consecutives spaces
at each end of line for a linebreak.

```shell
# content/contact/_index.md
---
title: "Contact"
date: 2021-02-12T23:02:40+01:00
draft: false
---

Write some text  
And give some contacts  
```

### Menu

Manage the menu into `config.toml` project main config file.

```shell
# config.toml

[menu]
  [[menu.main]]
    name = "Portfolio"
    url = "/"
    weight = 1
  [[menu.main]]
    name = "Contact"
    url = "/contact/"
    weight = 3
```

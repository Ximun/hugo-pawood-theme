const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const WebpackAssetsManifest = require('webpack-assets-manifest')

var config = {
  entry: {
    app: [ './assets/scss/main.scss', './assets/js/main.js' ]
  },
  output: {
    path: path.resolve('./static'),
    publicPath: '',
    filename: '[name].[chunkhash:8].js',
    chunkFilename: '[id]-[chunkhash].js'
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
          'resolve-url-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff2?|eot|ttf|otf|wav)(\?.*)?$/,
        type: 'asset/resource'
      },
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].[fullhash:8].css'
    }),
    new CleanWebpackPlugin({
      root: path.resolve('./'),
      verbose: true,
      dry: false
    }),
    new WebpackAssetsManifest({
      output: '../data/manifest.json',
      publicPath: true
    })
  ]
}

module.exports = config
